# Normalize Whitespace

See the [Whitespace Normalisation](https://docs.deltaxml.com/xdc/latest/samples/whitespace-normalisation) samples page for an explanation of these files.


## REST request:

Replace {LOCATION} below with your location of the download. 



```
<compare>
    <inputA xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/addressesA.xml</path>
    </inputA>
    <inputB xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/addressesB.xml</path>
    </inputB>
    <configuration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/config-nw-specific.xml</path>
    </configuration>
</compare>
```

See [XML Data Compare REST User Guide](https://docs.deltaxml.com/xdc/latest/rest-user-guide) for how to run the comparison using REST.